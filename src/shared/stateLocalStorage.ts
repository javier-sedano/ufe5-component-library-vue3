export function getState<T>(storageKey: string, defaultState: T): T {
    const state = localStorage?.getItem(storageKey);
    try {
        return (state ? JSON.parse(state) : defaultState);
    } catch (e) {
        console.error(e);
        return defaultState;
    }
}

export function updateState<T>(storageKey: string, defaultState: T, newState: Partial<T>) {
    let state = getState<T>(storageKey, defaultState);
    state = {
        ...state,
        ...newState,
    };
    localStorage.setItem(storageKey, JSON.stringify(state));
}
